GIT COMMIT NAMING CONVENTIONS
=============================

Everything here is based mainly on **AngularJS** conventions about committing comments.

SEE: `Grafikart
<https://www.youtube.com/watch?v=AlHohDBBAMY>` _.


============
Nomenclature
============
<type(scope): operations>
-------------------------

**<type>**

    BUILD
        anything about build system (gulp, webpack, npm, pip requirements...)

    CI
        anything about Continuous Integration (Jenkins, Travis, CI...)

    DOCS
        anything about documentation

    FEAT
        new features added into code

    FIX
        any bug fix

    PERF
        performance improvement

    REFACTOR
        any refactoring operation

    STYLE
        any change in code styling / design

    TEST
        any modification in test


**<scope>**
your intentions or any intervention zone.

**<operations>**
imperative style, no uppercase neither punctuation


============
ALTERNATIVES
============

It's possible to have multi-line for a better and complete report in commits.

::

    <type(scope): operations>\n
    <motivations>
    <footer>

**<motivations>**
In case of responding on any issue, be more descriptive about this particular commit.

**<footer>**
Put the issue number(#1987) and add any PR issue number too.

